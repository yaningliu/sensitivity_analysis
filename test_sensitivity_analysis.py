"""This is a test file for sensitivity_analysis.py

Time-stamp: <2016-08-17 12:26:24 yaningliu>
"""
import numpy as np
import sensitivity_analysis as sa
from scipy.stats import norm


def test_gsa(nparam, noutput, nsample, model, sa_method):
    """Saltelli's Sobol' global sensitivity analysis

    :param nparam: integer, the number of parameters
    :param noutput: integer, the number of outputs
    :param nsample: integer, the number of samples
    :param model: the function to be tested, e.g. tfunc.ishigami,
    tfunc.crestaux_poly
    :param sa_method: string, the sensitivity analysis method, e.g.,
    'sobol' or 'saltelli'
    :returns: gsa
    :rtype: sensitivity_analysis class

    """
    param_dist = ['uniform']*3

    param_scaled_set1 = np.random.rand(nsample, nparam)
    param_scaled_set2 = np.random.rand(nsample, nparam)

    gsa = sa.sensitivity_analysis(nparam, param_dist, noutput, model)

    if sa_method == 'saltelli':
        gsa.gsa_sobol_saltelli(nsample, param_scaled_set1, param_scaled_set2)
    if sa_method == 'sobol':
        gsa.gsa_sobol(nsample, param_scaled_set1, param_scaled_set2)

    print('First order sensitivity indices are:')
    print(gsa.S_fo)
    print('Total sensitivity indices are:')
    print(gsa.S_tot)


def test_migsa_dloop_cv(nparam, noutput, nsample, model, n_jobs=1, cv=None):
    """Borgonovo's moment-independent global sensitivity analysis
    The algorithm is double loop with cross validation for bandwidth

    :param nparam: integer, the number of parameters
    :param noutput: integer, the number of outputs
    :param nsample: integer, the number of samples
    :param model: the function to be tested, e.g. tfunc.ishigami,
    tfunc.crestaux_poly
    :param n_jobs: integer, the number of jobs
    :param cv: integer/None, the folds of cross-validation. If None, 3-folds
    :returns: migsa
    :rtype: sensitivity_analysis class

    """
    param_dist = ['uniform']*3

    param_scaled_set1 = np.random.rand(nsample, nparam)
    param_scaled_set2 = np.random.rand(nsample, nparam)

    gsa = sa.sensitivity_analysis(nparam, param_dist, noutput, model)

    gsa.migsa_double_loop_cv(nsample, param_scaled_set1, param_scaled_set2,
                             n_jobs=n_jobs, cv=cv)

    print('Sigma indices are:')
    print(gsa.sigma)


def test_migsa_dloop(nparam, noutput, nsample, model, band_width):
    """Borgonovo's moment-independent global sensitivity analysis
    The algorithm is double loop without cross validation for bandwidth

    :param nparam: integer, the number of parameters
    :param noutput: integer, the number of outputs
    :param nsample: integer, the number of samples
    :param model: the function to be tested, e.g. tfunc.ishigami,
    tfunc.crestaux_poly
    :param band_width: float, the bandwidth for kernel density
    :returns: migsa
    :rtype: sensitivity_analysis class

    """
    param_dist = ['uniform']*3

    param_scaled_set1 = np.random.rand(nsample, nparam)
    param_scaled_set2 = np.random.rand(nsample, nparam)

    gsa = sa.sensitivity_analysis(nparam, param_dist, noutput, model)

    gsa.migsa_double_loop(nsample, param_scaled_set1, param_scaled_set2,
                          band_width)

    print('Sigma indices are:')
    print(gsa.sigma)


def test_migsa_sloop_cv(nparam, noutput, nsample, model, n_jobs=1, cv=None):
    """Borgonovo's moment-independent global sensitivity analysis
    The algorithm is single loop with cross validation for bandwidth

    :param nparam: integer, the number of parameters
    :param noutput: integer, the number of outputs
    :param nsample: integer, the number of samples
    :param model: the function to be tested, e.g. tfunc.ishigami,
    tfunc.crestaux_poly
    :param n_jobs: integer, the number of jobs
    :param cv: integer/None, the folds of cross-validation. If None, 3-folds
    :returns: migsa
    :rtype: sensitivity_analysis class

    """
    param_dist = ['uniform']*3

    param_scaled_set = np.random.rand(nsample, nparam)
    # pdf_param_scaled_set = norm.pdf(norm.ppf(param_scaled_set))
    # pdf_param_scaled_set = np.ones((nsample, nparam))

    gsa = sa.sensitivity_analysis(nparam, param_dist, noutput, model)

    gsa.migsa_single_loop_cv(nsample, param_scaled_set, n_jobs=n_jobs, cv=cv)

    print('Sigma indices are:')
    print(gsa.sigma)


def test_migsa_sloop(nparam, noutput, nsample, model, band_width):
    """Borgonovo's moment-independent global sensitivity analysis
    The algorithm is single loop without cross validation for bandwidth

    :param nparam: integer, the number of parameters
    :param noutput: integer, the number of outputs
    :param nsample: integer, the number of samples
    :param model: the function to be tested, e.g. tfunc.ishigami,
    tfunc.crestaux_poly
    :param band_width: float, the bandwidth for kernel density
    :returns: migsa
    :rtype: sensitivity_analysis class

    """
    param_dist = ['uniform']*3

    param_scaled_set = np.random.rand(nsample, nparam)
    # pdf_param_scaled_set = norm.pdf(param_mapped_set)
    # pdf_param_scaled_set = np.ones((nsample, nparam))

    gsa = sa.sensitivity_analysis(nparam, param_dist, noutput, model)

    gsa.migsa_single_loop(nsample, param_scaled_set, band_width)

    print('Sigma indices are:')
    print(gsa.sigma)
