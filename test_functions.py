"""This module includes a bunch of test functions for
sensitivity analysis/reduce-order modeling/uncertainty analysis

Time-stamp: <2016-08-17 11:09:21 yaningliu>
"""

from math import pi
import numpy as np
from scipy.stats import norm


def ishigami(x, a=None, b=None, if_uniform01=True):
    """Evaluate Ishigami function

    :param x: nd array of size (nsample, 3), in the range of [0, 1]
    :param a: double, value a
    :param b: double, value b
    :param if_uniform01: if the input parameters are uniformly distributed in
    (0, 1). If False, the input x should be already in (-pi, pi)
    :returns: the values of Ishigami function
    :rtype: 1d array/scalar
    """
    if a is None:
        a = 7.0
    if b is None:
        b = 0.1

    if if_uniform01:
        # transform the parameters from [0, 1] to [-pi, pi]
        x_n = -pi + 2*pi*x
    else:
        x_n = x

    if x.ndim == 2:
        f = np.sin(x_n[:, 0]) + a*np.square(np.sin(x_n[:, 1])) + \
            b*np.power(x_n[:, 2], 4)*np.sin(x_n[:, 0])
    elif x.ndim == 1:
        f = np.sin(x_n[0]) + a*np.square(np.sin(x_n[1])) + \
            b*np.power(x_n[2], 4)*np.sin(x_n[0])

    return f


def crestaux_poly(x):
    """Crestaux polynomial function, which can be found at
    Crestaux et al, Polynomial chaos expansion for sensitivity analysis,
    RESS 94 (2009), 1161-1172

    :param x: nd array of size (nsample, 3), in the range of [0, 1]
    already standard normal
    :returns: the values of the polynomial function
    :rtype: 1d array or scalar

    """
    if x.ndim == 2:
        f = np.prod((2*x+1.0)/2, axis=1)
    elif x.ndim == 1:
        f = np.prod((2*x+1.0)/2)
    return f


def ishigami_crestauxpoly(x):
    """compute the ishigami and crestauxpoly functions as two-dimensional
    outputs

    :param x: nd array of size (nsample, 3), in the range of [0, 1]
    :returns: the values of ishigami and polynomial function
    :rtype: 2d array of size (nsample, 2)

    """
    f1 = ishigami(x)
    f2 = crestaux_poly(x)
    print(np.column_stack((f1, f2)))
    return np.column_stack((f1, f2))


def sum_of_normal_rv(x, if_uniform01=True):
    """compute the function which is sum of normal random variables given in
    Pengfei Wei et al, Monte Carlo simulation for moment-independent
    sensitivity analysis, RESS 110 (2013). The bandwidth is chosen using
    cross validation

    :param x: nd array of size (nsample, 6), in the range of [0, 1]
    :param if_uniform01: if the input parameters are uniformly distributed in
    (0, 1). If False, the input x should be already standard normal
    :returns: the function values
    :rtype: 1d array or scalar

    """
    # uniform to Gaussian
    if if_uniform01:
        x_gauss = norm.ppf(x)
    else:
        x_gauss = x

    coef = np.array([1.5, 1.6, 1.7, 1.8, 1.9, 2.0])
    if x_gauss.ndim == 1:
        f = coef[0]*x_gauss[0] + coef[1]*x_gauss[1] + coef[2]*x_gauss[2] + \
            coef[3]*x_gauss[3] + coef[4]*x_gauss[4] + coef[5]*x_gauss[5]
    elif x_gauss.ndim == 2:
        f = coef[0]*x_gauss[:, 0] + coef[1]*x_gauss[:, 1] + \
            coef[2]*x_gauss[:, 2] + coef[3]*x_gauss[:, 3] + \
            coef[4]*x_gauss[:, 4] + coef[5]*x_gauss[:, 5]
    return f
