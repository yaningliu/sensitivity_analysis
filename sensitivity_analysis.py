"""This module contains various sensitivity analysis methods,
including local, global and screening methods

Time-stamp: <2016-08-17 14:11:49 yaningliu>

__author__ = 'Yaning Liu'
__version__ = '0.1'
"""

import numpy as np
from sklearn.grid_search import GridSearchCV
from sklearn.neighbors import KernelDensity
from scipy.stats import gaussian_kde


class sensitivity_analysis(object):
    """ Performs sensitivity analysis. The methods include:
    Sobol' indices, moment independent global sensitivity measures,
    Morris' screening method,
    correlated input parameters, using Nataf transformation and copula
    """

    def __init__(self, nparam, param_dist, noutput, model):
        """The initializer of sensitivity analysis class

        :param nparam: integer, the number of input parameters
        :param param_dist: a list of strings, the distributions of the
        input parameters
        :param noutput: integer, the number of ouptut variables
        :param model: function, the model, the model takes a numpy array
        of size (nsample, nparam) in the range (0, 1) as input and the output
        is of size (nsample, noutputs)
        :returns:
        :rtype:

        """
        self.nparam = nparam
        self.param_dist = param_dist
        self.noutput = noutput
        self.model = model

    def migsa_double_loop_cv(self, nsample, param_scaled_set1,
                             param_scaled_set2, n_jobs=1, cv=None, **kwargs):
        """Use double loop algorithm described in
        Pengfei Wei et al, Monte Carlo simulation for moment-independent
        sensitivity analysis, RESS 110 (2013). The bandwidth is chosen using
        cross validation

        :param nsample, integer, the number of samples
        :param param_scaled_set1: numpy array of size (nsample, nparam) in the
        range of (0, 1)
        :param param_scaled_set2: numpy array of size (nsample, nparam) in the
        range of (0, 1), a second set of random numbers
        Here the sample sizes of param_scaled_set1 and
        param_scaled_set2 can be different
        :param n_jobs, integer, the number of jobs
        :param cv, integer, the number of folds for cross validation; if None,
        then use 3-fold cross validation
        :returns: the moment independent global sensitivity measure, known as
        delta indices
        :rtype: numpy array of size nparam

        """
        nsample1 = param_scaled_set1.shape[0]
        # nsample2 = param_scaled_set2.shape[0]

        simout1 = np.zeros((nsample1, self.noutput))
        simout1 = self.model(param_scaled_set1, **kwargs)

        if simout1.ndim == 1:
            simout1 = simout1[:, np.newaxis]

        # find the optimal bandwidth by grid search
        params = {'bandwidth': np.logspace(-2, 1, 20)}
        grid1 = [None]*self.noutput
        grid3 = [None]*self.noutput
        kde1 = [None]*self.noutput
        kde3 = [None]*self.noutput

        # cv_args = {}
        # if 'n_jobs' in kwargs.keys():
        #     cv_args['n_jobs'] = kwargs['n_jobs']
        # if 'cv' in kwargs.keys():
        #     cv_args['cv'] = kwargs['cv']

        for i in range(self.noutput):
            grid1[i] = GridSearchCV(KernelDensity(), params, n_jobs=n_jobs,
                                    cv=cv)
            grid1[i].fit(simout1[:, i][:, np.newaxis])
            kde1[i] = grid1[i].best_estimator_

        self.sigma = np.zeros((self.nparam, self.noutput))
        for i in range(self.nparam):
            s_hat = np.zeros((nsample1, self.noutput))
            for k in range(nsample1):
                param_scaled_set3 = param_scaled_set2.copy()
                param_scaled_set3[:, i] = np.array(
                    [param_scaled_set1[k, i]]*nsample)

                simout3 = self.model(param_scaled_set3, **kwargs)
                if simout3.ndim == 1:
                    simout3 = simout3[:, np.newaxis]

                for j in range(self.noutput):
                    grid3[j] = GridSearchCV(KernelDensity(), params,
                                            n_jobs=n_jobs, cv=cv)
                    grid3[j].fit(simout3[:, j][:, np.newaxis])
                    kde3[j] = grid3[j].best_estimator_

                    pdf_nume = np.exp(
                        kde1[j].score_samples(simout3[:, j][:, np.newaxis]))
                    pdf_deno = np.exp(
                        kde3[j].score_samples(simout3[:, j][:, np.newaxis]))
                    s_hat[k, j] = np.mean(np.abs(
                        pdf_nume/pdf_deno-1.0))

            self.sigma[i, :] = np.mean(s_hat, axis=0) / 2.0

    def migsa_double_loop(self, nsample, param_scaled_set1,
                          param_scaled_set2, band_width, **kwargs):
        """Use double loop algorithm described in
        Pengfei Wei et al, Monte Carlo simulation for moment-independent
        sensitivity analysis, RESS 110 (2013). The bandwidth is given, instead
        of using cross validation

        :param nsample, integer, the number of samples
        :param param_scaled_set1: numpy array of size (nsample, nparam) in the
        range of (0, 1)
        :param param_scaled_set2: numpy array of size (nsample, nparam) in the
        range of (0, 1), a second set of random numbers
        Here the sample sizes of param_scaled_set1 and
        param_scaled_set2 can be different
        :param n_jobs, integer, the number of jobs
        :param cv, integer, the number of folds for cross validation; if None,
        then use 3-fold cross validation
        :returns: the moment independent global sensitivity measure, known as
        delta indices
        :rtype: numpy array of size nparam

        """
        nsample1 = param_scaled_set1.shape[0]

        simout1 = np.zeros((nsample1, self.noutput))
        simout1 = self.model(param_scaled_set1, **kwargs)

        if simout1.ndim == 1:
            simout1 = simout1[:, np.newaxis]

        kde1 = [None]*self.noutput
        kde3 = [None]*self.noutput

        # cv_args = {}
        # if 'n_jobs' in kwargs.keys():
        #     cv_args['n_jobs'] = kwargs['n_jobs']
        # if 'cv' in kwargs.keys():
        #     cv_args['cv'] = kwargs['cv']

        for i in range(self.noutput):
            kde1[i] = KernelDensity(bandwidth=band_width)
            kde1[i].fit(simout1[:, i][:, np.newaxis])

        self.sigma = np.zeros((self.nparam, self.noutput))
        for i in range(self.nparam):
            s_hat = np.zeros((nsample1, self.noutput))
            for k in range(nsample1):
                param_scaled_set3 = param_scaled_set2.copy()
                param_scaled_set3[:, i] = np.array(
                    [param_scaled_set1[k, i]]*nsample)

                simout3 = self.model(param_scaled_set3, **kwargs)
                if simout3.ndim == 1:
                    simout3 = simout3[:, np.newaxis]

                for j in range(self.noutput):
                    kde3[j] = KernelDensity(bandwidth=band_width)
                    kde3[j].fit(simout3[:, j][:, np.newaxis])

                    pdf_nume = np.exp(
                        kde1[j].score_samples(simout3[:, j][:, np.newaxis]))
                    pdf_deno = np.exp(
                        kde3[j].score_samples(simout3[:, j][:, np.newaxis]))
                    s_hat[k, j] = np.mean(np.abs(
                        pdf_nume/pdf_deno-1.0))

            self.sigma[i, :] = np.mean(s_hat, axis=0) / 2.0

    def migsa_single_loop_cv(self, nsample, param_scaled_set, n_jobs=1,
                             cv=None, **kwargs):
        """Use single loop algorithm described in
        Pengfei Wei et al, Monte Carlo simulation for moment-independent
        sensitivity analysis, RESS 110 (2013). The bandwidth is chosen using
        cross validation

        :param nsample, integer, the number of samples
        :param param_scaled_set: numpy array of size (nsample, nparam) in the
        range of (0, 1)
        :param n_jobs, integer, the number of jobs
        :param cv, integer, the number of folds for cross validation; if None,
        then use 3-fold cross validation
        :returns: the moment independent global sensitivity measure, known as
        delta indices
        :rtype: numpy array of size nparam

        """
        nsample = param_scaled_set.shape[0]

        simout = np.zeros((nsample, self.noutput))
        simout = self.model(param_scaled_set, **kwargs)

        if simout.ndim == 1:
            simout = simout[:, np.newaxis]

        # find the optimal bandwidth by grid search
        params = {'bandwidth': np.logspace(-2, 1, 20)}
        grid1 = [None]*self.noutput
        kde1 = [None]*self.noutput
        grid2 = [None]*self.noutput
        kde2 = [None]*self.noutput

        # cv_args = {}
        # if 'n_jobs' in kwargs.keys():
        #     cv_args['n_jobs'] = kwargs['n_jobs']
        # if 'cv' in kwargs.keys():
        #     cv_args['cv'] = kwargs['cv']

        for i in range(self.noutput):
            grid1[i] = GridSearchCV(KernelDensity(), params, n_jobs=n_jobs,
                                    cv=cv)
            grid1[i].fit(simout[:, i][:, np.newaxis])
            kde1[i] = grid1[i].best_estimator_
            print("best bandwidth for output {0}: {1}"
                  .format(i, grid1[i].best_estimator_.bandwidth))

        # the probability density function values of param_scaled_set, so
        # it should be just a matrix of 1
        pdf_param_scaled_set = np.ones((nsample, self.nparam))

        self.sigma = np.zeros((self.nparam, self.noutput))

        for j in range(self.noutput):
            # sklearn uses the same bandwidth for each dimension of the
            # multivariate variables, so just estimate it once
            grid2[j] = GridSearchCV(KernelDensity(), params, n_jobs=n_jobs,
                                    cv=cv)
            grid2[j].fit(np.column_stack((simout[:, j],
                                          param_scaled_set[:, 0])))
            kde2[j] = grid2[j].best_estimator_
            print("best bandwidth for parameter {0}, output {1}: {2}"
                  .format(0, j, grid2[j].best_estimator_.bandwidth))
            for i in range(self.nparam):
                # grid2[j] = GridSearchCV(KernelDensity(), params, n_jobs=n_jobs,
                #                         cv=cv)
                # grid2[j].fit(np.column_stack((simout[:, j],
                #                               param_scaled_set[:, i])))
                # kde2[j] = grid2[j].best_estimator_
                # print("best bandwidth for parameter {0}, output {1}: {2}"
                #       .format(i, j, grid2[j].best_estimator_.bandwidth))

                # pdf of f_Y(y)
                pdf_nume1 = np.exp(
                    kde1[j].score_samples(simout[:, j][:, np.newaxis]))
                # pdf of f_Xi(xi)
                pdf_nume2 = pdf_param_scaled_set[:, i]
                # pdf of f_Y,Xi(y, xi)
                pdf_deno = np.exp(
                    kde2[j].score_samples(np.column_stack((
                        simout[:, j], param_scaled_set[:, i]))))

                self.sigma[i, j] = np.mean(np.abs(
                    pdf_nume1*pdf_nume2/pdf_deno-1)) / 2.0

    def migsa_single_loop(self, nsample, param_scaled_set, band_width,
                          **kwargs):
        """Use single loop algorithm described in
        Pengfei Wei et al, Monte Carlo simulation for moment-independent
        sensitivity analysis, RESS 110 (2013). The bandwidth is given, instead
        of being chosen using cross validation

        :param nsample, integer, the number of samples
        :param param_scaled_set: numpy array of size (nsample, nparam) in the
        range (0, 1)
        :param band_width, float, the bandwidth, e.g. 0.2
        :returns: the moment independent global sensitivity measure, known as
        delta indices
        :rtype: numpy array of size nparam

        """
        nsample = param_scaled_set.shape[0]

        simout = np.zeros((nsample, self.noutput))
        simout = self.model(param_scaled_set, **kwargs)

        if simout.ndim == 1:
            simout = simout[:, np.newaxis]

        kde1 = [None]*self.noutput
        kde2 = [None]*self.noutput

        for i in range(self.noutput):
            kde1[i] = KernelDensity(bandwidth=band_width)
            kde1[i].fit(simout[:, i][:, np.newaxis])

        # the probability density function values of param_scaled_set, so
        # it should be just a matrix of 1
        pdf_param_scaled_set = np.ones((nsample, self.nparam))

        self.sigma = np.zeros((self.nparam, self.noutput))

        for i in range(self.nparam):
            for j in range(self.noutput):
                kde2[j] = KernelDensity(bandwidth=band_width)
                kde2[j].fit(np.column_stack((simout[:, j],
                                             param_scaled_set[:, i])))

                # pdf of f_Y(y)
                pdf_nume1 = np.exp(
                    kde1[j].score_samples(simout[:, j][:, np.newaxis]))
                # pdf of f_Xi(xi)
                pdf_nume2 = pdf_param_scaled_set[:, i]
                # pdf of f_Y,Xi(y, xi)
                pdf_deno = np.exp(
                    kde2[j].score_samples(np.column_stack((
                        simout[:, j], param_scaled_set[:, i]))))

                self.sigma[i, j] = np.mean(np.abs(
                    pdf_nume1*pdf_nume2/pdf_deno-1)) / 2.0

    def gsa_sobol_saltelli(self, nsample, param_scaled_set1,
                           param_scaled_set2, **kwargs):
        """Compute Sobol' global sensitivity indices using
        Saltelli's algorithm

        :param nsample: integer, the number of samples
        :param param_scaled_set1, numpy array of size (nsample, nparam) in the
        range of (0, 1)
        :param param_scaled_set2, numpy array of size (nsample, nparam) in the
        range of (0, 1), a second set of random numbers
        :param kwargs, pass in the argument for model
        :returns: the main indices and total indices
        :rtype: two numpy arrays of size nparam

        """
        simout1 = np.zeros((nsample, self.noutput))
        simout2 = np.zeros((nsample, self.noutput))
        simout3 = np.zeros((nsample, self.noutput))

        simout1 = self.model(param_scaled_set1, **kwargs)
        f_mean = np.mean(simout1, axis=0)
        f_var = np.mean(simout1**2, axis=0) - f_mean**2

        simout2 = self.model(param_scaled_set2, **kwargs)
        f_mean2 = np.mean(simout1*simout2, axis=0)

        # first order indices and total indices
        self.S_fo = np.zeros((self.nparam, self.noutput))
        self.S_tot = np.zeros((self.nparam, self.noutput))

        for i in range(self.nparam):
            # first order indices
            param_scaled_set3 = param_scaled_set2.copy()
            param_scaled_set3[:, i] = param_scaled_set1[:, i]

            simout3 = self.model(param_scaled_set3, **kwargs)
            self.S_fo[i, :] = (np.mean(simout1*simout3, axis=0) -
                               f_mean2) / f_var

            # total indices
            self.S_tot[i, :] = 1.0 - (np.mean(simout2*simout3, axis=0)
                                      - f_mean2) / f_var

    def gsa_sobol(self, nsample, param_scaled_set1,
                  param_scaled_set2, **kwargs):
        """Compute Sobol' global sensitivity indices using
        Sobol's original algorithm

        :param nsample: integer, the number of samples
        :param param_scaled_set1, numpy array of size (nsample, nparam) in the
        range of (0, 1)
        :param param_scaled_set2, numpy array of size (nsample, nparam) in the
        range of (0, 1), a second set of random numbers
        :param kwargs, pass in the argument for model
        :returns: the main indices and total indices
        :rtype: two numpy arrays of size nparam

        """
        simout1 = np.zeros((nsample, self.noutput))
        simout3 = np.zeros((nsample, self.noutput))

        simout1 = self.model(param_scaled_set1, **kwargs)
        f_mean = np.mean(simout1, axis=0)
        f_var = np.mean(simout1**2, axis=0) - f_mean**2

        # first order indices and total indices
        self.S_fo = np.zeros((self.nparam, self.noutput))
        self.S_tot = np.zeros((self.nparam, self.noutput))

        for i in range(self.nparam):
            # first order indices
            param_scaled_set3 = param_scaled_set2.copy()
            param_scaled_set3[:, i] = param_scaled_set1[:, i]

            simout3 = self.model(param_scaled_set3, **kwargs)

            self.S_fo[i, :] = (np.mean(simout1*simout3, axis=0) -
                               f_mean**2) / f_var

            # total indices
            param_scaled_set3 = param_scaled_set1.copy()
            param_scaled_set3[:, i] = param_scaled_set2[:, i]

            simout3 = self.model(param_scaled_set3, **kwargs)

            self.S_tot[i, :] = np.mean(0.5*(simout1-simout3)**2,
                                       axis=0) / f_var
